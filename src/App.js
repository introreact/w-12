import './App.css';
import { AiFillGithub } from 'react-icons/ai';
import { AiFillLinkedin } from 'react-icons/ai';

function App() {
  return (
    <div className="App container">
      <header className="App-header">
        <img src="img/vamklogo.png" className="App-logo" alt="logo" />
        <div className="info">
          <h2><span style={{color:'#990099'}}>Hung Nguyen</span></h2>
          <p><span style={{fontWeight:'bold'}}>Student</span></p>
          <p><span style={{color:'#990099'}}>Information Technology</span></p>
          <p><span style={{color:'#990099'}}>hungnguyen150300@gmail.com</span></p>
          <p><span style={{color:'#990099'}}>+358 46 590 3165</span></p>
          <p><span style={{color:'#990099'}}>Vuorikatu 14-18, FI-65100 VAASA, Finland</span></p>
        </div>
        <div className="social">
          <a href="https://github.com/hung1503" className="social-link"><AiFillGithub /></a>
          <a href="https://www.linkedin.com/in/hung-nguyen-b9a1891a7/" className="social-link"><AiFillLinkedin /></a>
        </div>
      </header>
    </div>
  );
}

export default App;
